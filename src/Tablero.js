import React, { useState } from "react";
import piezas from "./Piezas";
import { Button } from "react-bootstrap";
import "./Tablero.css";
import Chessboard from "chessboardjsx-custom";
import Xarrow from "react-xarrows/lib";
import { FaChevronRight, FaChevronLeft } from "react-icons/fa";
import { objecttofsy, objecttomerida } from "./functio";
import Actions from "./Actions";

var arr = [
  "wP",
  "wR",
  "wN",
  "wB",
  "wQ",
  "wK",
  "bP",
  "bR",
  "bN",
  "bB",
  "bQ",
  "bK",
];

function Lines(props) {
  const arrows = props.lines.map((line, index) => (
    <div key={index}>
      <Xarrow
        start={line.slice(0, 2)}
        end={line.slice(2, 4)}
        startAnchor="middle"
        endAnchor="middle"
        curveness={0}
        color={line.slice(4, line.length)}
      />
    </div>
  ));

  return <div>{arrows}</div>;
}

const Tablero = (props) => {
  const [position, setPosition] = useState({});
  const [pieza, setPieza] = useState("wP");
  const [arrow, setArrow] = useState("");
  const [lines, setLines] = useState([]);
  const [color, setColor] = useState("#00FF0B");
  const [merida, setMerida] = useState("");
  const [partidas, setPartidas] = useState("");
  const [indexPartida, setIndexPartida] = useState(-1);
  const [data, setData] = useState("");
  const [fsy, setFsy] = useState("");
  const [aux, setAux] = useState(false);
  const componentRef = React.createRef();

  function setPos(pos) {
    setPosition(pos);
  }

  function update(e) {
    var tmp = position;

    if (tmp[e] === undefined) {
      tmp[e] = pieza;
    } else {
      delete tmp[e];
    }
    setPosition(tmp);
    setMerida(objecttomerida(position));
    setFsy(objecttofsy(position));
  }

  function addline(e) {
    if (arrow !== "") {
      var tmp = arrow + e + color;
      setLines((lines) => [...lines, tmp]);
      setArrow("");
    } else {
      setArrow(...arrow, e);
    }
  }

  function removeline(e) {
    var tmp = lines;
    tmp = tmp.slice(0, tmp.length - 1);
    setLines(tmp);
    setArrow("");
  }

  function move(e) {
    e.preventDefault();
    let jugada = partidas[indexPartida][0];
    if (jugada === undefined) {
      return;
    }
    if (jugada.length === 4) {
      let tmp = position;
      var piece = tmp[jugada.slice(0, 2)];
      delete tmp[jugada.slice(0, 2)];
      tmp[jugada.slice(2, 4)] = piece;
      setPosition(tmp);
      setPosition(tmp);
    }
    if (jugada.length === 5) {
      let tmp = position;
      tmp[jugada.slice(3, 5)] = tmp[jugada.slice(1, 3)];
      delete tmp[jugada.slice(1, 3)];
      setPosition(tmp);
      setPosition(tmp);
    }
    var tmp = partidas;
    tmp[indexPartida] = tmp[indexPartida].slice(1, tmp[indexPartida].length);
    setPartidas(tmp);
    console.log(partidas);
    console.log(position);
    addline("a8");
    removeline();
    setAux(!aux);
  }

  function start() {
    setPosition({
      a1: "wR",
      b1: "wN",
      c1: "wB",
      d1: "wQ",
      e1: "wK",
      f1: "wB",
      g1: "wN",
      h1: "wR",
      a2: "wP",
      b2: "wP",
      c2: "wP",
      d2: "wP",
      e2: "wP",
      f2: "wP",
      g2: "wP",
      h2: "wP",
      a8: "bR",
      b8: "bN",
      c8: "bB",
      d8: "bQ",
      e8: "bK",
      f8: "bB",
      g8: "bN",
      h8: "bR",
      a7: "bP",
      b7: "bP",
      c7: "bP",
      d7: "bP",
      e7: "bP",
      f7: "bP",
      g7: "bP",
      h7: "bP",
    });
  }

  return (
    <div id="outer">
      <div id="tablero">
        {indexPartida === -1 ? (
          <div id="black">
            {arr.slice(6, 13).map((item, index) => (
              <Button
                variant="light"
                size="lg"
                onClick={(e) => setPieza(item)}
                key={index}
              >
                {piezas[item]}
              </Button>
            ))}
          </div>
        ) : (
          <div
            style={{ width: `100%`, margin: `2rem auto`, maxWidth: `500px` }}
          >
            <b> Partida:</b>
            <input
              type="number"
              max={partidas.length - 1}
              onChange={(e) => {
                setIndexPartida(e.target.value);
                start();
              }}
              value={indexPartida}
              min={0}
            ></input>
            <br />
            {data[indexPartida * 2]}
          </div>
        )}
        <div
          ref={componentRef}
          style={{ boxShadow: `0 8px 16px 0 rgba(0,0,0,0.2)` }}
        >
          {aux ? (
            <Chessboard
              key={position}
              onSquareClick={(e) => update(e)}
              position={position}
              calcWidth={(e) => (e.screenWidth = 500)}
              onSquareRightClick={(e) => addline(e)}
              id="chess"
              darkSquareStyle={{ backgroundColor: `#0062A8` }}
              lightSquareStyle={{ backgroundColor: `white` }}
              transitionDuration={300}
            />
          ) : (
            <Chessboard
								position={position}
              calcWidth={(e) => (e.screenWidth = 500)}
              onSquareRightClick={(e) => addline(e)}
              darkSquareStyle={{ backgroundColor: `#0062A8` }}
              lightSquareStyle={{ backgroundColor: `white` }}
              transitionDuration={300}
            />
          )}
          <Lines lines={lines} />
        </div>
        {indexPartida === -1 ? (
          <div id="white">
            {arr.slice(0, 6).map((item, index) => (
              <Button
                variant="light"
                size="lg"
                onClick={(e) => setPieza(item)}
                key={index}
              >
                {piezas[item]}
              </Button>
            ))}
          </div>
        ) : (
          <div style={{ width: `25%`, margin: `2rem auto` }}>
            <Button variant="light" size="lg">
              <FaChevronLeft />
            </Button>{" "}
            <Button variant="light" size="lg" onClick={(e) => move(e)}>
              <FaChevronRight />
            </Button>
          </div>
        )}
        {indexPartida === -1 ? (
          <></>
        ) : (
          <div>
            <textarea
              value={partidas[indexPartida]}
              cols="65"
              rows="5"
              style={{ maxWidth: `500px`, borderRadius: `1em` }}
            />
          </div>
        )}
      </div>
      <Actions
        pieza={pieza}
        componentRef={componentRef}
        removeline={removeline}
        setPos={setPos}
        position={position}
        setPosition={setPosition}
        setColor={setColor}
        merida={merida}
        fsy={fsy}
        color={color}
        setPartidas={setPartidas}
        setIndexPartida={setIndexPartida}
        start={start}
        setData={setData}
      />
    </div>
  );
};

export default Tablero;
