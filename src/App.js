import React from "react";
import "./App.css";
import Tablero from "./Tablero";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Tablero />
    </div>
  );
}

export default App;
