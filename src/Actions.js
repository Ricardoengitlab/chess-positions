import React from "react";
import {
  exportComponentAsJPEG,
  exportComponentAsPDF,
  exportComponentAsPNG,
} from "react-component-export-image";
import { Button, Dropdown } from "react-bootstrap";
import { fsytoobject } from "./functio";
import piezas from "./Piezas";

const Actions = (props) => {
  var fileReader;

  function handleFileRead(e) {
    e.preventDefault();
    var content = fileReader.result;
    content = content.replace(/[^A-Za-z0-9]/g, "");
    var readPosition = {};
    readPosition = fsytoobject(content);
    props.setPosition(readPosition);
    document.getElementById("file").value = null;
  }

  function handleFileChosen(file) {
    fileReader = new FileReader();
    fileReader.onloadend = handleFileRead;
    fileReader.readAsText(file);
  }

  function handleClick(e) {
    e.preventDefault();
    document.getElementById("file").click();
  }

  function handleFileReadPGN(e) {
    e.preventDefault();
    var content = fileReader.result;
    content = content.replace("\n", "");
    content = content.split("\r");

    var s = "";
    var index = 0;
    var t = [];
    while (index < content.length) {
      if (content[index] === "\n") {
        t.push(s);
        s = "";
        index++;
      } else {
        s += content[index];
        index++;
      }
    }

    props.setData(t);
    var partidas = [];
    for (let index = 1; index < t.length; index += 2) {
      t[index] = t[index].split("\n").join(" ");
      t[index] = t[index].split("{").join(" ");
      t[index] = t[index].split("}").join(" ");
      partidas.push(t[index].split(" "));
    }
    console.log(partidas);

    const regex2 = /^(R|K|N|Q|B)[a-h][1-8][a-h][1-8]/;
    const regex3 = /^[a-h][1-8][a-h][1-8]/;

    for (let index = 0; index < partidas.length; index++) {
      partidas[index] = partidas[index].filter(
        // (str) => regex.test(str) || regex2.test(str) || regex3.test(str)
        (str) => regex2.test(str) || regex3.test(str)
      );
    }
    console.log(partidas);
    props.start();
    props.setPartidas(partidas);
    props.setIndexPartida(0);
    document.getElementById("pgn").value = null;
  }

  function handleFileChosenPGN(file) {
    fileReader = new FileReader();
    fileReader.onloadend = handleFileReadPGN;
    fileReader.readAsText(file);
  }

  function handlePGN(e) {
    e.preventDefault();
    document.getElementById("pgn").click();
  }

  return (
    <div id="actions">
      <input
        type="file"
        id="file"
        className="input-file"
        accept=".fsy"
        onChange={(e) => handleFileChosen(e.target.files[0])}
        style={{ display: `none` }}
      />
      <input
        type="file"
        id="pgn"
        className="input-file"
        accept=".pgn"
        onChange={(e) => handleFileChosenPGN(e.target.files[0])}
        style={{ display: `none` }}
      />
      <div style={{ textAlign: `center` }}>
        <p>Pieza seleccionada: </p>
        {piezas[props.pieza]}
      </div>
      <div style={{ display: `flex`, margin: `2rem auto 0` }}>
        <p>Color de línea:</p>
        <input
          type="color"
          value={props.color}
          onChange={(e) => props.setColor(e.target.value)}
        />
      </div>
      <Button variant="dark" onClick={(e) => props.removeline(e)}>
        Borrar última linea
      </Button>
      <Button variant="dark" onClick={(e) => handleClick(e)}>
        Leer posición
      </Button>
      <Button variant="dark" onClick={(e) => handlePGN(e)}>
        Leer PGN
      </Button>
      <Dropdown>
        <Dropdown.Toggle variant="dark" id="dropdown-basic">
          Salvar Posición
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item
            href={
              "data:application/octet-stream," +
              encodeURIComponent(props.merida)
            }
            download="diagrama.mrq"
          >
            Chess Merida
          </Dropdown.Item>
          <Dropdown.Item
            href={
              "data:application/octet-stream," + encodeURIComponent(props.fsy)
            }
            download="diagrama.fsy"
          >
            FSY
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <Button variant="dark" onClick={(e) => props.setPosition({})}>
        Nueva posición
      </Button>
      <Dropdown>
        <Dropdown.Toggle variant="dark" id="dropdown-basic">
          Capturar Imagen
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item
            onClick={() =>
              exportComponentAsJPEG(props.componentRef, "diagrama.jpeg")
            }
          >
            Export As JPEG
          </Dropdown.Item>
          <Dropdown.Item
            onClick={() =>
              exportComponentAsPDF(props.componentRef, "diagrama.pdf")
            }
          >
            Export As PDF
          </Dropdown.Item>
          <Dropdown.Item
            onClick={() =>
              exportComponentAsPNG(props.componentRef, "diagrama.png")
            }
          >
            Export As PNG
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default Actions;
