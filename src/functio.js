const letters = ["a", "b", "c", "d", "e", "f", "g", "h"];

const fsyobject = {
  p: `bP`,
  t: `bR`,
  c: `bN`,
  a: `bB`,
  d: `bQ`,
  r: `bK`,
  P: `wP`,
  T: `wR`,
  C: `wN`,
  A: `wB`,
  D: `wQ`,
  R: `wK`,
};

const objectmerida = {
  wP: "p",
  bP: "o",
  wR: "r",
  bR: "t",
  wN: "n",
  bN: "m",
  wB: "b",
  bB: "v",
  wQ: "q",
  bQ: "w",
  wK: "k",
  bK: "l",
};

const objectfsy = {
  wP: "P",
  bP: "p",
  wR: "T",
  bR: "t",
  wN: "C",
  bN: "c",
  wB: "A",
  bB: "a",
  wQ: "D",
  bQ: "d",
  wK: "R",
  bK: "r",
};

export function getKeyByValue(object, value) {
  return Object.keys(object).find((key) => object[key] === value);
}

export function fsytoobject(fsy) {
  fsy = fsy.split("\n").join("");
  var s = {};

  var count = 0;
  for (let index = 8; index >= 1; index--) {
    for (let letter = 0; letter < 8; letter++) {
      if (fsy[count] !== "1") {
        let pos = letters[letter] + index;
        s[pos] = fsyobject[fsy[count]];
      }
      count++;
    }
  }

  return s;
}

export function objecttomerida(object) {
  var aux = `!""""""""#\n`;
  for (let index = 8; index >= 1; index--) {
    var s = "$";
    for (let column = 0; column < letters.length; column++) {
      let pos = letters[column] + index;
      if (index % 2 === 0) {
        // Linea inicia con cuadrado blanco
        if (object[pos] === undefined) {
          s = column % 2 === 0 ? s + " " : s + "+";
        } else {
          s =
            column % 2 === 0
              ? s + objectmerida[object[pos]]
              : s + objectmerida[object[pos]].toUpperCase();
        }
      } else {
        // Linea inicia con cuadrado negro
        console.log(pos, object[pos]);
        if (object[pos] === undefined) {
          s = column % 2 === 0 ? s + "+" : s + " ";
        } else {
          s =
            column % 2 === 0
              ? s + objectmerida[object[pos]].toUpperCase()
              : s + objectmerida[object[pos]];
        }
      }
    }
    aux = aux + s + "%\n";
  }
  aux = aux + "/(((((((()";

  return aux;
}

export function objecttofsy(object) {
  var aux = "";
  for (let index = 8; index >= 1; index--) {
    var s = "";
    for (let column = 0; column < letters.length; column++) {
      let pos = letters[column] + index;
      // Linea inicia con cuadrado blanco
      if (object[pos] === undefined) {
        s = s + "1";
      } else {
        s = s + objectfsy[object[pos]];
      }
    }
    aux = aux + s + "\n";
  }
  return aux;
}
